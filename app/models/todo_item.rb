class TodoItem < ApplicationRecord
  validates :content, presence: true,
                      length: { maximum: 100 }
  validates :priority, presence: true, numericality: { :greater_than_or_equal_to => 0 }
end

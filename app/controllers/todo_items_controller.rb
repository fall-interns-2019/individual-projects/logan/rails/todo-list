class TodoItemsController < ActionController::API
  # I S N E C U D - index, show, new, edit, create, update, destroy
  # C R U D - create, read, update, destroy
  def index
    render json: TodoItem.all
  end

  def create
    item = TodoItem.new(todo_item_params)

    if item.save
      render json: item.attributes
    else
      render json: { success: false, message: 'Failed to create new todo item' }
    end
  end

  def update
    item = TodoItem.find(params[:id])

    if item.update(todo_item_params)
      render json: item.attributes
    else
      render json: { success: false, message: 'Failed to update todo item with id ' + params[:id] }
    end
  end

  def update_multiple
    new_priorities = {}

    params.require(:_json).each do |param|
      if param.require [:id, :priority]
        param.permit!
        new_priorities[param[:id]] = param[:priority]
      end
    end

    new_priorities.keys.each do |id|
      TodoItem.find(id).update!(priority: new_priorities[id])
    end

    render json: TodoItem.all
  end

  def destroy
    item = TodoItem.find(params[:id])

    if item.destroy
      render json: item.attributes
    else
      render json: { success: false, message: 'Failed to destroy todo item by id ' + params[:id] }
    end
  end

  private

  def todo_item_params
    params.require(:todo_item).permit(:content, :priority)
  end
end

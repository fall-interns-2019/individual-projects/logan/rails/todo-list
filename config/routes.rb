Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'todo_items#index'

  resources :todo_items

  put 'todo_items', to: 'todo_items#update_multiple'
end

class UpdateTodoItems < ActiveRecord::Migration[5.1]
  def change
    add_column :todo_items, :priority, :integer
  end
end
